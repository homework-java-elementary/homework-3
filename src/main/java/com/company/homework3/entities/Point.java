package com.company.homework3.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double distanceTo(Point p) {
        if(p==null){
            throw new IllegalArgumentException("Невозможно найти расстояние до null");
        }
        double dx = p.x - x;
        double dy = p.y - y;
        double distanceSquare = dx * dx + dy * dy;
        return Math.sqrt(distanceSquare);
    }
}
