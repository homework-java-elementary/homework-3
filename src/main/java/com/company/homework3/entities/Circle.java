package com.company.homework3.entities;

import com.company.homework3.entities.Point;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString @Setter @Getter
public class Circle {
    private Point centre = null;
    private double radius = 0;

    public Circle() {
    }

    public Circle(Point centre, double radius) {
        this.centre = centre;
        this.radius = radius;
    }

    public boolean containsPoint(Point point) {
        double distance = centre.distanceTo(point);
        return distance <= radius;
    }
}
