package com.company.homework3;

import com.company.homework3.entities.Circle;
import com.company.homework3.services.InMemoryPointsService;
import com.company.homework3.services.PointsService;
import com.company.homework3.ui.menu.Menu;
import com.company.homework3.ui.menu.MenuItem;
import com.company.homework3.ui.menu.items.*;
import com.company.homework3.ui.views.CircleView;
import com.company.homework3.ui.views.PointsView;
import com.company.homework3.utilities.Container;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PointsView pointsView = new PointsView(scanner);
        CircleView circleView = new CircleView(scanner);
        PointsService pointsService = new InMemoryPointsService();
        Container<Circle> circleContainer = new Container<>();
        Menu menu = new Menu(new MenuItem[]{
                new AddPointMenuItem(pointsService, pointsView),
                new ChangeCircleMenuItem(circleContainer, circleView),
                new PointsInsideCircleMenuItem(pointsService, pointsView, circleContainer),
                new PointsOutsideCircleMenuItem(pointsService, pointsView, circleContainer),
                new ExitMenuItem()
        }, scanner);
        menu.execute();
    }
}
