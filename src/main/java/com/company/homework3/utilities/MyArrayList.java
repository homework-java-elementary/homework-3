package com.company.homework3.utilities;

public class MyArrayList<T> {
    private Object[] objects = new Object[0];

    public void addObject(T object) {
        Object[] newObjects = new Object[objects.length + 1];
        for (int i = 0; i < objects.length; i++) {
            newObjects[i] = objects[i];
        }
        newObjects[newObjects.length - 1] = object;
        objects = newObjects;
    }

    public T get(int index) {
        return (T) objects[index];
    }

    public int size(){
        return objects.length;
    }
}
