package com.company.homework3.utilities;

public class Container<T> {
    private Object object;

    public T getObject() {
        return (T) object;
    }

    public void setObject(T object) {
        this.object = object;
    }
}
