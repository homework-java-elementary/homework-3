package com.company.homework3.services;

import com.company.homework3.entities.Circle;
import com.company.homework3.entities.Point;
import com.company.homework3.utilities.MyArrayList;

public interface PointsService {
    void addPoint(Point point);

    MyArrayList<Point> getAllPoints();

    MyArrayList<Point> getPointsInsideCircle(Circle circle);

    MyArrayList<Point> getPointsOutsideCircle(Circle circle);
}