package com.company.homework3.services;

import com.company.homework3.entities.Circle;
import com.company.homework3.entities.Point;
import com.company.homework3.utilities.MyArrayList;

public class InMemoryPointsService implements PointsService{
    private MyArrayList<Point> myArrayList = new MyArrayList<>();

    @Override
    public void addPoint(Point point) {
        myArrayList.addObject(point);
    }

    @Override
    public MyArrayList<Point> getAllPoints() {
        return myArrayList;
    }

    @Override
    public MyArrayList<Point> getPointsInsideCircle(Circle circle) {
        MyArrayList<Point> result = new MyArrayList<>();
        for (int i = 0; i < myArrayList.size(); i++) {
            if(circle.containsPoint(myArrayList.get(i))){
                result.addObject(myArrayList.get(i));
            }
        }
        return result;
    }

    @Override
    public MyArrayList<Point> getPointsOutsideCircle(Circle circle) {
        MyArrayList<Point> result = new MyArrayList<>();
        for (int i = 0; i < myArrayList.size(); i++) {
            if(!circle.containsPoint(myArrayList.get(i))){
                result.addObject(myArrayList.get(i));
            }
        }
        return result;
    }
}
