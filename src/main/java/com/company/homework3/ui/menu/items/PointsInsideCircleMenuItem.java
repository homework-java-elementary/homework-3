package com.company.homework3.ui.menu.items;


import com.company.homework3.entities.Circle;
import com.company.homework3.services.PointsService;
import com.company.homework3.ui.menu.MenuItem;
import com.company.homework3.ui.views.PointsView;
import com.company.homework3.utilities.Container;

public class PointsInsideCircleMenuItem implements MenuItem {
    private PointsService pointsService;
    private PointsView pointsView;
    private Container<Circle> circleContainer;

    public PointsInsideCircleMenuItem(PointsService pointsService, PointsView pointsView, Container<Circle> circleContainer) {
        this.pointsService = pointsService;
        this.pointsView = pointsView;
        this.circleContainer = circleContainer;
    }

    @Override
    public String getName() {
        return "Points inside circle";
    }

    @Override
    public void execute() {
        if (pointsService.getAllPoints().size() == 0) {
            System.out.println("No points");
            return;
        }

        if (circleContainer.getObject() == null) {
            System.out.println("Mo circle");
            return;
        }

        System.out.println("Points inside circle:");
        pointsView.show(pointsService.getPointsInsideCircle(circleContainer.getObject()));
        System.out.println();
    }
}
