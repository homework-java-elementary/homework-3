package com.company.homework3.ui.menu;

import java.util.Scanner;

public class Menu {
    MenuItem[] items;
    Scanner scanner;

    public Menu(MenuItem[] items, Scanner scanner) {
        this.items = items;
        this.scanner = scanner;
    }

    public void execute() {
        while (true) {
            showMenu();
            int choice = getChoice();
            if (choice < 0 || choice >= items.length) {
                System.out.println("Incorrect choice");
                continue;
            }
            items[choice].execute();
            if (items[choice].isFinal()) {
                break;
            }
        }
    }

    private void showMenu() {
        System.out.println("----------------------------------------");
        for (int i = 0; i < items.length; i++) {
            System.out.printf("%2d - %s\n", i + 1, items[i].getName());
        }
        System.out.println("----------------------------------------");
    }

    private int getChoice() {
        System.out.print("Select menu item: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        return choice - 1;
    }
}
