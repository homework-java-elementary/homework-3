package com.company.homework3.ui.menu.items;

import com.company.homework3.entities.Circle;
import com.company.homework3.services.PointsService;
import com.company.homework3.ui.menu.MenuItem;
import com.company.homework3.ui.views.PointsView;
import com.company.homework3.utilities.Container;

public class PointsOutsideCircleMenuItem implements MenuItem {
    private PointsService pointsService;
    private PointsView pointsView;
    private Container<Circle> circleContainer;

    public PointsOutsideCircleMenuItem(PointsService pointsService, PointsView pointsView, Container<Circle> circleContainer) {
        this.pointsService = pointsService;
        this.pointsView = pointsView;
        this.circleContainer = circleContainer;
    }

    @Override
    public String getName() {
        return "Points outside circle";
    }

    @Override
    public void execute() {
        if (pointsService.getAllPoints().size() == 0) {
            System.out.println("No points");
            return;
        }

        if (circleContainer.getObject() == null) {
            System.out.println("Mo circle");
            return;
        }

        System.out.println("Points outside circle:");
        pointsView.show(pointsService.getPointsOutsideCircle(circleContainer.getObject()));
        System.out.println();
    }
}
