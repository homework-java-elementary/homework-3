package com.company.homework3.ui.menu;

public interface MenuItem {
    String getName();

    void execute();

    default boolean isFinal() {
        return false;
    }
}
