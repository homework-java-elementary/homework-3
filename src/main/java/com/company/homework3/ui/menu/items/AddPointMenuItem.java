package com.company.homework3.ui.menu.items;

import com.company.homework3.services.PointsService;
import com.company.homework3.ui.menu.MenuItem;
import com.company.homework3.ui.views.PointsView;

public class AddPointMenuItem implements MenuItem {
    private PointsService pointsService;
    private PointsView pointsView;

    public AddPointMenuItem(PointsService pointsService, PointsView pointsView) {
        this.pointsService = pointsService;
        this.pointsView = pointsView;
    }

    @Override
    public String getName() {
        return "Add point";
    }

    @Override
    public void execute() {
        pointsService.addPoint(pointsView.readPoint());
    }
}
