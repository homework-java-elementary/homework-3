package com.company.homework3.ui.menu.items;

import com.company.homework3.entities.Circle;
import com.company.homework3.ui.menu.MenuItem;
import com.company.homework3.ui.views.CircleView;
import com.company.homework3.utilities.Container;

public class ChangeCircleMenuItem implements MenuItem {
    CircleView circleView;
    Container<Circle> circleContainer;

    public ChangeCircleMenuItem(Container<Circle> circleContainer, CircleView circleView) {
        this.circleContainer = circleContainer;
        this.circleView = circleView;
    }

    @Override
    public String getName() {
        return "Change parameters of circle";
    }

    @Override
    public void execute() {
        circleContainer.setObject(circleView.readCircle());
    }
}
