package com.company.homework3.ui.views;

import com.company.homework3.entities.Circle;
import com.company.homework3.entities.Point;

import java.util.Scanner;

public class CircleView {
    private Scanner scanner;

    public CircleView(Scanner scanner) {
        this.scanner = scanner;
    }

    public Circle readCircle() {
        System.out.println("Enter coordinates centre of circle:");

        double x = getDoubleFromConsole("Enter x:", false);

        double y = getDoubleFromConsole("Enter y:", false);

        double r = getDoubleFromConsole("Enter radius of circle:", true);

        return new Circle(new Point(x, y), r);
    }

    private double getDoubleFromConsole(String text, boolean onlyPositive) {
        if (onlyPositive) {
            double number;
            do {
                System.out.print(text);
                while (!scanner.hasNextDouble()) {
                    scanner.nextLine();
                    System.out.print(text);
                }
                number = scanner.nextDouble();
                scanner.nextLine();
            } while (number <= 0);
            return number;
        } else {
            System.out.print(text);
            while (!scanner.hasNextDouble()) {
                scanner.nextLine();
                System.out.print(text);
            }
            return scanner.nextDouble();
        }
    }
}
