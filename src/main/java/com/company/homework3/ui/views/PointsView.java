package com.company.homework3.ui.views;

import com.company.homework3.entities.Point;
import com.company.homework3.services.PointsService;
import com.company.homework3.utilities.MyArrayList;

import java.util.Scanner;

public class PointsView {
    private Scanner scanner;

    public PointsView(Scanner scanner) {
        this.scanner = scanner;
    }

    public void show(MyArrayList<Point> myArrayList) {
        System.out.println("---------------------------Points---------------------------");
        for (int i = 0; i < myArrayList.size(); i++) {
            show(myArrayList.get(i));
        }
        System.out.println("---------------------------Points---------------------------");
        System.out.println();
    }

    public void show(Point point) {
        System.out.println(point);
    }

    public Point readPoint() {
        System.out.println("Enter coordinates point:");

        double x = getDoubleFromConsole("Enter x:");

        double y = getDoubleFromConsole("Enter y:");

        return new Point(x, y);
    }

    private double getDoubleFromConsole(String text) {
        System.out.print(text);
        while (!scanner.hasNextDouble()) {
            scanner.nextLine();
            System.out.print(text);
        }
        return scanner.nextDouble();
    }
}
