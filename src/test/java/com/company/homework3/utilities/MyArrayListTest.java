package com.company.homework3.utilities;

import com.company.homework3.entities.Point;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {
    @Test
    public void testSize() {
        MyArrayList<Point> myArrayList = new MyArrayList<>();
        assertEquals(0, myArrayList.size());
        myArrayList.addObject(new Point(0, 0));
        myArrayList.addObject(new Point(1, 1));
        assertEquals(2, myArrayList.size());
    }

    @Test
    public void TestGet() {
        MyArrayList<Point> myArrayList = new MyArrayList<>();
        Point p1 = new Point(0, 0);
        Point p2 = new Point(2, 2);
        myArrayList.addObject(p1);
        myArrayList.addObject(p2);
        assertEquals(p1, myArrayList.get(0));
        assertEquals(p2, myArrayList.get(1));
    }

    @Test
    public void testAddObject() {
        MyArrayList<Point> myArrayList = new MyArrayList<>();
        Point p1 = new Point(0, 0);
        myArrayList.addObject(p1);
        assertEquals(p1, myArrayList.get(0));
        Point p2 = new Point(2, 2);
        myArrayList.addObject(p2);
        assertEquals(p2, myArrayList.get(1));
    }
}