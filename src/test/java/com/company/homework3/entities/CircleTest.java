package com.company.homework3.entities;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {
    @Test @DisplayName("принадлежность точки окружности")
    public void testContainsPoint() {
        Circle circle = new Circle(new Point(0, 0), 5);
        assertAll(
                () -> assertTrue(circle.containsPoint(new Point(0, 5))),
                () -> assertTrue(circle.containsPoint(new Point(5, 0))),
                () -> assertTrue(circle.containsPoint(new Point(0, 0))),
                () -> assertTrue(circle.containsPoint(new Point(2, 2))),
                () -> assertTrue(circle.containsPoint(new Point(3, 2))),
                () -> assertFalse(circle.containsPoint(new Point(5, 5))),
                () -> assertFalse(circle.containsPoint(new Point(10, 10))),
                () -> assertFalse(circle.containsPoint(new Point(1, 5))),
                () -> assertFalse(circle.containsPoint(new Point(5, 1)))
        );
    }

}