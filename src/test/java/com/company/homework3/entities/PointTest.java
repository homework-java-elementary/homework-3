package com.company.homework3.entities;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {
    @Test @DisplayName("расстояние между точками")
    public void testDistanceTo() {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(3, 4);
        Point p3 = new Point(4, 3);
        Point p4 = new Point(0, 4);
        Point p5 = new Point(4, 0);

        assertAll(
                () -> assertEquals(0, p1.distanceTo(p1)),
                () -> assertEquals(5, p1.distanceTo(p2)),
                () -> assertEquals(5, p1.distanceTo(p3)),
                () -> assertEquals(4, p1.distanceTo(p4)),
                () -> assertEquals(4, p1.distanceTo(p5)),
                () -> assertEquals(0, p5.distanceTo(p5))
        );
        assertThrows(IllegalArgumentException.class, () -> p1.distanceTo(null));
    }
}